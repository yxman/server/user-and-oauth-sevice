//
// Created by thephosphorus on 10/8/19.
//

#ifndef USER_SERVICE_INVALIDUSEREXCEPTION_H
#define USER_SERVICE_INVALIDUSEREXCEPTION_H

#include <exception>

class InvalidUserException : std::exception {};

#endif // USER_SERVICE_INVALIDUSEREXCEPTION_H
