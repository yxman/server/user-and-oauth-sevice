//
// Created by thephosphorus on 10/6/19.
//

#include "User.h"
#include <iostream>

const std::string &User::username() const { return _username; }

void User::setUsername(const std::string &username) { _username = username; }

void User::setPassword(const std::string &password) { _password = password; }

const std::string &User::role() const { return _role; }

void User::setRole(const std::string &role) { _role = role; }

bool User::isPassword(const std::string &password) const {
  return password == _password;
}

const std::string &User::password() const { return _password; }

User::User(std::string &&username, std::string &&password, std::string &&role)
    : _username(username), _password(password), _role(role) {}
