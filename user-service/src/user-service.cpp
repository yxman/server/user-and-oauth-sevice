﻿// user-service.cpp : Defines the entry point for the application.
//
#include "User.h"
#include "UserPermissions.h"
#include "UserRequestHandler.h"

#include <yxlib/ConfigHelper.h>
#include <yxlib/DBManager.h>
#include <yxlib/RestServer.h>
#include <yxlib/yxstream.h>

#include <string>

using namespace yx;

constexpr auto UserConfigFile = "/etc/yx/user/config.yaml";

void helloWorld(const Rest::Request &req, Rest::Response res) {
  res.send(restbed::OK, "HelloWorld");
}

int main() {

  // Add Config files
  ConfigHelper::addFile(UserConfigFile);

  // Config Helper
  DBManager db;

  UserPermissions users(&db);
  UserRequestHandler::use(&users);

  // Create Server
  RestServer server;
  // Add Endpoints (this will be refactored after for a better endpoint
  // handeling
  server.get("/usager/hello", helloWorld);
  server.post("/usager/login", UserRequestHandler::connect);
  server.post("/usager/logout", UserRequestHandler::disconnect);
  server.post("/usager/motdepasse", UserRequestHandler::changePassword);
  server.post("/usager/admin/motdepasse",
              UserRequestHandler::changeOtherAccountPassword);
  server.post("/usager/admin/creationcompte", UserRequestHandler::createUser);
  server.del("/usager/admin/suppressioncompte", UserRequestHandler::deleteUser);
  server.get("usager/admin/usagers", UserRequestHandler::getUsers);

  yx::cout() << "Le serveur écoute sur " << server.getHost()
            << " sur le port " << std::to_string(server.getPort()) << std::endl;
  server.start();
  return EXIT_SUCCESS;
}
