//
// Created by thephosphorus on 10/31/19.
//

#ifndef USER_SERVICE_USERREQUESTHANDLER_H
#define USER_SERVICE_USERREQUESTHANDLER_H

#include "yxlib/RestServer.h"
class UserPermissions;

class UserRequestHandler {
public:
  static void use(UserPermissions *userPermission);

  static void connect(const yx::Rest::Request &req, yx::Rest::Response &res);
  static void disconnect(const yx::Rest::Request &req, yx::Rest::Response &res);
  static void changePassword(const yx::Rest::Request &req,
                             yx::Rest::Response &res);
  static void changeOtherAccountPassword(const yx::Rest::Request &req,
                                         yx::Rest::Response &res);
  static void createUser(const yx::Rest::Request &req, yx::Rest::Response &res);
  static void deleteUser(const yx::Rest::Request &req, yx::Rest::Response &res);
  static void getUsers(const yx::Rest::Request &req, yx::Rest::Response &res);

private:
  static UserPermissions *_userPermissions;
};

#endif // USER_SERVICE_USERREQUESTHANDLER_H
