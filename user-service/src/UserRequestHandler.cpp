//
// Created by thephosphorus on 10/31/19.
//

#include "UserRequestHandler.h"
#include "UserPermissions.h"
#include <yxlib/oauthHandler.h>
#include <yxlib/yxstream.h>

#include <cpr/cpr.h>

using namespace yx;

constexpr auto connectEndpoint = "oauth/connect";
constexpr auto disconnectEndpoint = "oauth/disconnect";
constexpr auto usernameEndpoint = "oauth/username";

UserPermissions *UserRequestHandler::_userPermissions = nullptr;

void UserRequestHandler::use(UserPermissions *userPermissions) {
  _userPermissions = userPermissions;
}

void UserRequestHandler::connect(const yx::Rest::Request &req,
                                 yx::Rest::Response &res) {
  constexpr auto userField = "usager";
  constexpr auto passField = "mot_de_passe";

  Rest::Json body = req.body();

  if (!body.contains(userField) || !body.contains(passField)) {
    // Malformed request
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  const std::string username = body.at(userField);
  const std::string pass = body.at(passField);

  constexpr auto userTokenField = "userToken";
  constexpr auto userGroupField = "userGroup";
  constexpr auto usernameField = "username";

  User user = _userPermissions->getUser(username);

  if (user.username().empty() || !user.isPassword(pass)) {
    res.returnStatus(restbed::FORBIDDEN);
    return;
  }

  Rest::Json newBody;

  const std::string accessToken = OauthHandler::generateToken();

  newBody[userTokenField] = accessToken;
  newBody[usernameField] = username;
  newBody[userGroupField] = user.role();

  auto oAuthReq =
      cpr::Post(cpr::Url{OauthHandler::getOauthHost() + connectEndpoint},
                cpr::Body{newBody.dump()},
                cpr::Header{{"Content-Type", "application/json"}});

  constexpr auto isEditionModeField = "edition";
  constexpr auto accessTokenField = "accessToken";

  Rest::Json returnBody;

  returnBody[isEditionModeField] = (user.role() == "editor");
  returnBody[accessTokenField] = accessToken;

  yx::cout() << "Le jeton " << accessToken << " est assigné à l'utilisateur " << username
             << std::endl;
  res.sendJson(oAuthReq.status_code, returnBody);
}

void UserRequestHandler::disconnect(const yx::Rest::Request &req,
                                    yx::Rest::Response &res) {
  const std::string accessToken = req.accessToken();

  if (accessToken.empty()) {
    // Malformed request
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  constexpr auto userTokenField = "userToken";

  Rest::Json newBody;

  newBody[userTokenField] = accessToken;

  yx::cout() << "Déconnexion de " << accessToken << " ." << std::endl;

  auto oAuthReq =
      cpr::Post(cpr::Url{OauthHandler::getOauthHost() + disconnectEndpoint},
                cpr::Body{newBody.dump()},
                cpr::Header{{"Content-Type", "application/json"}});

  res.returnStatus(oAuthReq.status_code);
}

void UserRequestHandler::changePassword(const yx::Rest::Request &req,
                                        yx::Rest::Response &res) {
  constexpr auto oldPassField = "ancien";
  constexpr auto newPassField = "nouveau";
  const std::string accessToken = req.accessToken();

  Rest::Json body = req.body();

  if (!body.contains(oldPassField) || !body.contains(newPassField) ||
      accessToken.empty()) {
    // Malformed request
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  // Get username
  constexpr auto userTokenField = "userToken";
  Rest::Json newBody;
  newBody[userTokenField] = accessToken;

  auto oAuthReq =
      cpr::Get(cpr::Url{OauthHandler::getOauthHost() + usernameEndpoint},
               cpr::Body{newBody.dump()},
               cpr::Header{{"Content-Type", "application/json"}});

  if (oAuthReq.status_code != restbed::OK) {
    res.send(restbed::UNAUTHORIZED, "Invalid Token");
    return;
  }

  constexpr auto usernameField = "username";

  Rest::Json answer = Rest::Json::parse(oAuthReq.text);

  const auto username = answer[usernameField];

  const std::string oldPass = body.at(oldPassField);
  const std::string newPass = body.at(newPassField);

  const bool replaced =
      _userPermissions->changePass(username, oldPass, newPass);

  if (replaced) {
    res.returnStatus(restbed::OK);
  } else {
    res.send(restbed::UNAUTHORIZED, "Could not update user");
  }
}

void UserRequestHandler::changeOtherAccountPassword(
    const yx::Rest::Request &req, yx::Rest::Response &res) {

  const std::string userToken = req.accessToken();

  if (!OauthHandler::isvalidToken(userToken)) {
    yx::cout() << userToken
               << " n'a pas les permissions pour changer le mot de passe des autres utilisateurs"
               << std::endl;
    res.send(restbed::FORBIDDEN, "You need Admin access");
    return;
  }

  constexpr auto userField = "usager";
  constexpr auto oldPassField = "ancien";
  constexpr auto newPassField = "nouveau";

  Rest::Json body = req.body();

  if (!body.contains(userField) || !body.contains(oldPassField) ||
      !body.contains(newPassField)) {
    // Malformed request
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  const std::string username = body.at(userField);
  const std::string oldPass = body.at(oldPassField);
  const std::string newPass = body.at(newPassField);

  const bool replaced =
      _userPermissions->changePass(username, oldPass, newPass);

  if (replaced) {
    res.returnStatus(restbed::OK);
  } else {
    res.send(restbed::UNAUTHORIZED, "Could not update user");
  }
}

void UserRequestHandler::createUser(const yx::Rest::Request &req,
                                    yx::Rest::Response &res) {

  const std::string userToken = req.accessToken();

  if (!OauthHandler::isvalidToken(userToken)) {
    yx::cout() << userToken << " n'a pas les permissions pour creer des comptes"
               << std::endl;
    res.send(restbed::FORBIDDEN, "You need Admin access");
    return;
  }

  constexpr auto userField = "usager";
  constexpr auto passField = "mot_de_passe";
  constexpr auto editionModeField = "edition";

  Rest::Json body = req.body();

  if (!body.contains(userField) || !body.contains(passField) ||
      !body.contains(editionModeField) ||
      !body.at(editionModeField).is_boolean()) {
    // Malformed request
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  const std::string username = body.at(userField);
  const std::string password = body.at(passField);
  bool editionMode = body.at(editionModeField).get<bool>();
  const std::string role = editionMode ? "editor" : "user";

  User newUser;
  newUser.setUsername(username);
  newUser.setPassword(password);
  newUser.setRole(role);

  _userPermissions->addUser(newUser);
  res.returnStatus(restbed::OK);
}

void UserRequestHandler::deleteUser(const yx::Rest::Request &req,
                                    yx::Rest::Response &res) {

  const std::string userToken = req.accessToken();

  if (!OauthHandler::isvalidToken(userToken)) {
    yx::cout() << userToken << " n'a pas les permissions pour supprimer des comptes"
               << std::endl;
    res.send(restbed::FORBIDDEN, "You need Admin access");
    return;
  }

  constexpr auto userField = "usager";

  Rest::Json body = req.body();

  if (!body.contains(userField)) {
    // Malformed request
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  const std::string username = body.at(userField);

  _userPermissions->deleteUser(username);

  res.returnStatus(restbed::OK);
}
void UserRequestHandler::getUsers(const yx::Rest::Request &req,
                                  yx::Rest::Response &res) {
  std::vector<User> users = _userPermissions->getAllUsers();

  constexpr auto usersField = "comptes";
  constexpr auto userField = "usager";
  constexpr auto editionModeField = "edition";

  Rest::Json resBody;

  for (const User &user : users) {
    Rest::Json userJson;

    userJson[userField] = user.username();
    userJson[editionModeField] =
        (user.role() == "editor" || user.role() == "admin");
    resBody[usersField].push_back(userJson);
  }

  res.sendJson(restbed::OK, resBody);
}
