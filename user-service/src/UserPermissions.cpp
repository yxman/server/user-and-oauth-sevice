//
// Created by thephosphorus on 10/6/19.
//

#include "UserPermissions.h"

#include <yxlib/DBManager.h>

#include <string>

using namespace yx;

const std::string tableName = "users";
constexpr auto DbStringMaxSize = 100;
const auto DbStringMaxSize_str = std::to_string(DbStringMaxSize);

namespace soci {
template <> struct type_conversion<User> {
  typedef values base_type;

  static void from_base(values const &v, indicator /* ind */, User &p) {
    p.setUsername(v.get<std::string>("username"));
    p.setRole(v.get<std::string>("role"));
    p.setPassword(v.get<std::string>("password"));
  }

  static void to_base(const User &p, values &v, indicator &ind) {
    v.set("username", p.username());
    v.set("role", p.role());
    v.set("password", p.password());
    ind = i_ok;
  }
};
} // namespace soci

using namespace soci;

void UserPermissions::addUser(User &user) {
  if (!validUser(user.username())) {
    _dbManager->sql() << "INSERT INTO " + tableName +
                             " (username,role,password) "
                             " VALUES(:username, :role, :password)",
        use(user);
  }
}

bool UserPermissions::validUser(const std::string &username) const {
  std::string gottenUsername;
  indicator ind;
  _dbManager->sql() << "SELECT username FROM " + tableName +
                           " WHERE username = :username",
      use(username), into(gottenUsername, ind);
  return ind == i_ok && !gottenUsername.empty();
}

User UserPermissions::getUser(const std::string &username) const {
  User user;
  _dbManager->sql() << "SELECT * FROM " + tableName +
                           " WHERE username = :username",
      use(username), into(user);

  return user;
}

UserPermissions::UserPermissions(DBManager *dbManager) : _dbManager(dbManager) {
  _dbManager->sql() << "CREATE TABLE IF NOT EXISTS " + tableName +
                           " ("
                           "    username varchar(" +
                           DbStringMaxSize_str +
                           ") UNIQUE,"
                           "    role varchar(" +
                           DbStringMaxSize_str +
                           "),"
                           "    password varchar(" +
                           DbStringMaxSize_str +
                           ")"
                           ")";

  // Check if an admin account exists

  User admin;
  _dbManager->sql() << "SELECT * FROM " + tableName + " WHERE role = 'admin'",
      into(admin);

  if (admin.username().empty()) {
    // Add Default userAdmin
    admin.setRole("admin");
    admin.setUsername("admin");
    admin.setPassword("toor");
    addUser(admin);
  }
}

bool UserPermissions::changePass(const std::string &username,
                                 const std::string &oldPass,
                                 const std::string &newPass) {
  User user = getUser(username);

  if (user.username().empty() || !user.isPassword(oldPass)) {
    return false;
  }

  user.setPassword(newPass);

  return updateUser(user);
}

bool UserPermissions::updateUser(const User &user) {
  if (validUser(user.username())) {
    _dbManager->sql() << "UPDATE " + tableName +
                             " SET role = :role, password = :password"
                             " WHERE username = :username",
        use(user);
    return true;
  }

  return false;
}

std::vector<User> UserPermissions::getAllUsers() const {
  size_t nbUsers = getNumberUsers();
  std::vector<std::string> users;
  users.resize(nbUsers);
  _dbManager->sql() << "SELECT username FROM " + tableName, into(users);

  std::vector<User> usersObj;
  usersObj.reserve(users.size());
  for (const std::string &username : users) {
    usersObj.push_back(getUser(username));
  }

  return usersObj;
}
size_t UserPermissions::getNumberUsers() const {
  size_t number;
  _dbManager->sql() << "SELECT COUNT(*) FROM " + tableName, into(number);

  return number;
}
void UserPermissions::deleteUser(const std::string &username) const {
  _dbManager->sql() << "DELETE FROM " + tableName +
                           " WHERE username = :username",
      use(username);
}
