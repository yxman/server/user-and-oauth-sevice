//
// Created by thephosphorus on 10/6/19.
//

#ifndef USER_SERVICE_USER_H
#define USER_SERVICE_USER_H

#include <set>
#include <string>
#include <vector>

class User {
public:
  User(std::string &&username, std::string &&password, std::string &&email);
  User() = default;

  // Getter/Setters
  [[nodiscard]] const std::string &username() const;
  void setUsername(const std::string &username);

  [[nodiscard]] bool isPassword(const std::string &password) const;
  [[nodiscard]] const std::string &password() const;
  void setPassword(const std::string &password);

  [[nodiscard]] const std::string &role() const;
  void setRole(const std::string &email);

private:
  std::string _username;
  std::string _password;
  std::string _role;
};

#endif // USER_SERVICE_USER_H
