//
// Created by thephosphorus on 10/6/19.
//

#ifndef USER_SERVICE_USERPERMISSIONS_H
#define USER_SERVICE_USERPERMISSIONS_H

#include "User.h"

#include <yxlib/DBManager.h>

#include <unordered_map>

class UserPermissions {
public:
  explicit UserPermissions(yx::DBManager *dbManager);

  void addUser(User &user);
  bool updateUser(const User &user);
  [[nodiscard]] bool validUser(const std::string &username) const;
  [[nodiscard]] User getUser(const std::string &username) const;
  bool changePass(const std::string &username, const std::string &oldPass,
                  const std::string &newPass);
  [[nodiscard]] std::vector<User> getAllUsers() const;
  [[nodiscard]] size_t getNumberUsers() const;
  void deleteUser(const std::string &username) const;

private:
  yx::DBManager *_dbManager;
};

#endif // USER_SERVICE_USERPERMISSIONS_H
