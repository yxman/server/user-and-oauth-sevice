//
// Created by thephosphorus on 11/3/19.
//

#include "RequestHandler.h"
#include "SessionHandler.h"
#include <yxlib/yxstream.h>

using namespace yx;

void RequestHandler::connect(const yx::Rest::Request &req,
                             yx::Rest::Response &res) {

  constexpr auto userTokenField = "userToken";
  constexpr auto userGroupField = "userGroup";
  constexpr auto usernameField = "username";

  Rest::Json body = req.body();

  if (!body.contains(userTokenField) || !body.contains(userGroupField) || !body.contains(usernameField)) {
    // Malformed request
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  const std::string userToken = body.at(userTokenField);
  const std::string userGroup = body.at(userGroupField);
  const std::string username = body.at(usernameField);

  SessionHandler::getInstance().addToken(userToken, username,userGroup);

  yx::cout() << "Connexion du jeton utilisateur " << userToken << " avec le groupe " << userGroup << std::endl;

  res.returnStatus(restbed::OK);
}
void RequestHandler::disconnect(const yx::Rest::Request &req,
                                yx::Rest::Response &res) {
  constexpr auto userTokenField = "userToken";

  Rest::Json body = req.body();

  if (!body.contains(userTokenField)) {
    // Malformed request
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  const std::string userToken = body.at(userTokenField);

  if (SessionHandler::getInstance().removeToken(userToken))
  {
    yx::cout() << "Deconnexion " << userToken << std::endl;
    res.returnStatus(restbed::OK);
    return;
  }
  else {
    yx::cout() << "Utilisateur " << userToken << " non connecté." << std::endl;
    res.returnStatus(restbed::UNAUTHORIZED);
    return;
  }
}
void RequestHandler::verify(const yx::Rest::Request &req,
                             yx::Rest::Response &res) {
  constexpr auto userTokenField = "userToken";
  constexpr auto clientTokenField = "clientToken";

  Rest::Json body = req.body();

  if (!body.contains(userTokenField) || !body.contains(clientTokenField)) {
    // Malformed request
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  const std::string userToken = body.at(userTokenField);
  const std::string clientToken = body.at(clientTokenField);

  const bool isValid =
      SessionHandler::isValidConnection(userToken, clientToken);

  yx::cout() << "Vérifiaction de l'utilisateur " << userToken << " avec le client " << clientToken
            << " est valide: " << isValid << std::endl;

  res.returnStatus(isValid ? restbed::OK : restbed::FORBIDDEN);
}

void RequestHandler::getUsername(const yx::Rest::Request &req,
                             yx::Rest::Response &res) {

  constexpr auto userTokenField = "userToken";


  Rest::Json body = req.body();

  if (!body.contains(userTokenField)) {
    // Malformed request
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  const std::string userToken = body.at(userTokenField);

  yx::cout() << "Recherche de l'utilisateur " << userToken << std::endl;

  if (!SessionHandler::getInstance().tokenExists(userToken))
  {
    res.returnStatus(restbed::UNAUTHORIZED);
    return;
  }

  const std::string username = SessionHandler::getInstance().getUserName(userToken);

  yx::cout() << "Utilisateur trouvé: " << username << " pour le jeton: " << userToken << std::endl;

  Rest::Json retBody;

  constexpr auto usernameField = "username";

  retBody[usernameField] = username;

  res.sendJson(restbed::OK, retBody);
}
