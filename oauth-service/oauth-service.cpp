//
// Created by thephosphorus on 10/31/19.
//

#include "SessionHandler.h"
#include "RequestHandler.h"

#include <yxlib/ConfigHelper.h>
#include <yxlib/RestServer.h>
#include <yxlib/yxstream.h>


constexpr auto configFile = "/etc/yx/oauth/config.yaml";

using namespace yx;

void helloWorld(const Rest::Request &req, Rest::Response res) {
  res.send(restbed::OK, "HelloWorld");
}

int main()
{
  ConfigHelper::addFile(configFile);

  RestServer server;

  server.get("/oauth/hello", helloWorld);
  server.post("/oauth/connect", RequestHandler::connect);
  server.post("/oauth/disconnect", RequestHandler::disconnect);
  server.get("/oauth/verify", RequestHandler::verify);
  server.get("/oauth/username", RequestHandler::getUsername);



  yx::cout() << "OAuth écoute sur " << server.getHost() << "  sur le port " << server.getPort() << std::endl;
  server.start();
}