//
// Created by thephosphorus on 11/3/19.
//

#include "SessionHandler.h"
#include <algorithm>
#include <iostream>
#include <yxlib/ConfigHelper.h>

using namespace yx;

std::unique_ptr<SessionHandler> SessionHandler::_instance;

SessionHandler &SessionHandler::getInstance() {
  if (!_instance) {
    _instance = std::unique_ptr<SessionHandler>(new SessionHandler());
  }

  return *_instance;
}
void SessionHandler::addToken(const std::string &token,
                              const std::string &username,
                              const std::string &group) {
  if (!tokenExists(token)) {
    _currentTokens.emplace(token, group);
    _currentUsernames.emplace(token,username);
  }
}
bool SessionHandler::removeToken(const std::string &token) {
  if (tokenExists(token)) {
    _currentTokens.erase(token);
    _currentUsernames.erase(token);
    return true;
  }
  return false;
}
bool SessionHandler::tokenExists(const std::string &token) const {
  return _currentTokens.find(token) != _currentTokens.end();
}
void SessionHandler::addClientToGroup(const std::string &clientToken,
                                      const std::string &group) {
  auto it = _groupPermissions.begin();
  if ((it = _groupPermissions.find(group)) == _groupPermissions.end()) {
    _groupPermissions.emplace(group, std::unordered_set<std::string>());
    it = _groupPermissions.find(group);
  }

  it->second.emplace(clientToken);
}
bool SessionHandler::isValidConnection(const std::string &userToken,
                                       const std::string &clientToken) {
  const SessionHandler &sessionHandler = getInstance();

  if (!sessionHandler.tokenExists(userToken)) {
    return false;
  }

  const std::string group =
      sessionHandler._currentTokens.find(userToken)->second;

  // Check if the group exists
  auto it = sessionHandler._groupPermissions.begin();
  if ((it = sessionHandler._groupPermissions.find(group)) !=
      sessionHandler._groupPermissions.end()) {
    const std::unordered_set<std::string> &clientsToken = it->second;

    return clientsToken.find(clientToken) != clientsToken.end();
  } else {
    return false;
  }
}
SessionHandler::SessionHandler() {
  const auto &section = ConfigHelper::getSection("oauth");

  std::vector<ConfigSection> services = section.getConfigArr("services");

  for (const ConfigSection &service : services) {
    const std::string &client_token = service.getStr("client_token");
    const std::vector<std::string> groups =
        service.getStrArr("authorized_roles");
    std::unordered_set<std::string> groupSet;
    groupSet.reserve(groups.size());

    for (const std::string &group : groups) {
      addClientToGroup(client_token, group);
    }
  }
}
std::string SessionHandler::getUserName(const std::string &token) const {
  const auto it = _currentUsernames.find(token);

  return (it != _currentUsernames.end()) ? it->second : "";
}
