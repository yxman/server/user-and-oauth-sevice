//
// Created by thephosphorus on 11/3/19.
//

#ifndef OAUTH_SERVICE_SESSIONHANDLER_H
#define OAUTH_SERVICE_SESSIONHANDLER_H

#include <unordered_map>
#include <unordered_set>
#include <memory>

class SessionHandler {
public:
  static SessionHandler& getInstance();

  void addToken(const std::string& token, const std::string& username , const std::string& group);

  std::string getUserName(const std::string& token) const;

  bool removeToken(const std::string& token);

  bool tokenExists(const std::string& token) const;

  void addClientToGroup(const std::string& clientToken, const std::string& group);

  static bool isValidConnection(const std::string& userToken, const std::string& clientToken);

private:
  SessionHandler();

private:
  static std::unique_ptr<SessionHandler> _instance;
  std::unordered_map<std::string, std::string> _currentTokens;
  std::unordered_map<std::string, std::string> _currentUsernames;
  std::unordered_map<std::string, std::unordered_set<std::string>> _groupPermissions;
};

#endif // OAUTH_SERVICE_SESSIONHANDLER_H
