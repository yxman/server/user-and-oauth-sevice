//
// Created by thephosphorus on 11/3/19.
//

#ifndef OAUTH_SERVICE_REQUESTHANDLER_H
#define OAUTH_SERVICE_REQUESTHANDLER_H

#include <yxlib/RestServer.h>

class RequestHandler {
public:
  static void connect(const yx::Rest::Request &req, yx::Rest::Response &res);
  static void disconnect(const yx::Rest::Request &req, yx::Rest::Response &res);
  static void verify(const yx::Rest::Request &req, yx::Rest::Response &res);
  static void getUsername(const yx::Rest::Request &req, yx::Rest::Response &res);
};

#endif // OAUTH_SERVICE_REQUESTHANDLER_H
